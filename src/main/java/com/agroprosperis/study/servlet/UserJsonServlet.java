package com.agroprosperis.study.servlet;

import com.agroprosperis.study.model.User;
import com.agroprosperis.study.storage.JdbcStorage;
import com.agroprosperis.study.storage.Storage;
import com.agroprosperis.study.storage.UserStorage;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObjectBuilder;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

/**
 * Created by ruslan on 18.02.16.
 */
public class UserJsonServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        Storage<User> storage = new JdbcStorage();
        User user = new User();
        user.setName("Пользователь " + new Random(100).toString());

        //storage.getAll().add(user);

        JsonArrayBuilder array = Json.createArrayBuilder();

        for(User item: storage.getAll()){
            JsonObjectBuilder json = Json.createObjectBuilder();
            json.add("name", item.getName());
            json.add("object", item.getClass().toString());
            array.add(json);
        }

        resp.setContentType("text/json");
        resp.setCharacterEncoding("utf-8");
        PrintWriter out = resp.getWriter();
        out.print(array.build());
        out.flush();

    }
}
