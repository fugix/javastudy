package com.agroprosperis.study.servlet;

import com.agroprosperis.study.service.Properties;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ruslan on 18.02.16.
 */
public class UserServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        List<String> items = new ArrayList();
        items.add("Пользователь 1");
        items.add("Пользователь 2");
        items.add("Пользователь 3");
        items.add("Пользователь 4");
        items.add("Пользователь 5");
        items.add(Properties.getInstance().getValue("db.login"));


        req.setAttribute("items", items);
        RequestDispatcher dispatcher = req.getRequestDispatcher("/views/users.jsp");
        dispatcher.forward(req, resp);

    }
}
