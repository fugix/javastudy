package com.agroprosperis.study;

/**
 * Created by ruslan on 17.02.16.
 */
public class Car {

    private String engine;
    private String color;

    public String getEngine() {
        return engine;
    }

    public void setEngine(String engine) {
        this.engine = engine;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Car(CarBuilder builder) {
        this.color = builder.getColor();
        this.engine = builder.getEngine();
    }
}
