package com.agroprosperis.study.commands;

import com.agroprosperis.study.exceptions.BadCommandException;

/**
 * Created by ruslan on 17.02.16.
 */
public interface Icommand {
    public void execute() throws BadCommandException;
}
