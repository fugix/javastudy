package com.agroprosperis.study.exceptions;

/**
 * Created by ruslan on 17.02.16.
 */
public class BadCommandException extends Exception {

    public BadCommandException(String message) {
        super(message);
        System.out.println("Возникла ошибка");
    }
}
