package com.agroprosperis.study.storage;

import com.agroprosperis.study.model.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ruslan on 18.02.16.
 */
public class UserStorage implements Storage<User> {

    public List<User> getAll() {
        return userList;
    }

    public User getByName(String name) {
        return null;
    }

    public void close() {

    }

    private static UserStorage userStorage;

    private List<User> userList = new ArrayList<User>();

    public static UserStorage getInstance(){
        if (userStorage == null){
            userStorage = new UserStorage();
            return userStorage;
        }
        return userStorage;
    }

    public void setUserList(List<User> userList) {
        this.userList = userList;
    }
}
