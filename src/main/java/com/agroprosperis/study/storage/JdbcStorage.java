package com.agroprosperis.study.storage;

import com.agroprosperis.study.model.User;
import com.agroprosperis.study.service.Properties;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ruslan on 18.02.16.
 */
public class JdbcStorage implements Storage<User> {

    private Connection connection;

    private Connection getConnection() {

        try {
            Class.forName("com.mysql.jdbc.Driver");
            return DriverManager.getConnection(Properties.getInstance().getValue("db.url"), Properties.getInstance().getValue("db.login"), Properties.getInstance().getValue("db.password"));
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return null;
    }

    public JdbcStorage() {
        this.connection = getConnection();
    }

    public List<User> getAll() {

        List<User> userList = new ArrayList<User>();
        try {
            PreparedStatement ps = connection.prepareStatement("select * from users");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                User user = new User();
                user.setName(rs.getString("userName"));
                userList.add(user);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return userList;
        }
        close();

        return userList;
    }

    public User getByName(String name) {
        return null;
    }

    public void close() {
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

}
