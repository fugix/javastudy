package com.agroprosperis.study.storage;

import java.util.List;

/**
 * Created by ruslan on 18.02.16.
 */
public interface Storage<T> {

    public List<T> getAll();
    public T getByName(String name);
    public void close();


}
