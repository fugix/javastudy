package com.agroprosperis.study;

/**
 * Created by ruslan on 17.02.16.
 */
public class CarBuilder {

    private String engine;
    private String color;

    public String getEngine() {
        return engine;
    }

    public CarBuilder setEngine(String engine) {
        this.engine = engine;
        return this;
    }

    public String getColor() {
        return color;
    }

    public CarBuilder setColor(String color) {
        this.color = color;
        return this;
    }

    public Car build(){
        return new Car(this);
    }
}
