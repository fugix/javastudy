package com.agroprosperis.study;

import com.agroprosperis.study.commands.Icommand;
import com.agroprosperis.study.exceptions.BadCommandException;

import javax.servlet.http.HttpServlet;

/**
 * Created by ruslan on 17.02.16.
 */
public class Executor {
    private Icommand command;

    public Executor(Icommand command) throws BadCommandException {
        this.command = command;
        this.command.execute();
    }

}
