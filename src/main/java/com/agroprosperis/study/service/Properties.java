package com.agroprosperis.study.service;

import java.io.FileInputStream;
import java.io.IOException;

/**
 * Created by ruslan on 18.02.16.
 */
public class Properties {

    private static Properties INSTANCE = new Properties();
    private java.util.Properties prop = new java.util.Properties();

    private Properties(){

        try {
            prop.load(new FileInputStream(this.getClass().getClassLoader().getResource("properties").getFile()));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static Properties getInstance(){
        return INSTANCE;
    }
    public String getValue(String key){
        return prop.getProperty(key);
    }
}
