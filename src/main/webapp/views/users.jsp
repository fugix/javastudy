<%@ page import="java.util.ArrayList" %>
<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:h="http://java.sun.com/jsf/html">
<html lang="en">
<head>


    <meta charset="UTF-8">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <title></title>
</head>
<body>

<%
    ArrayList<String> nameList = new ArrayList<String>(4);
    nameList.add("David");
    nameList.add("Raymond");
    nameList.add("Beth");
    nameList.add("Joyce");
    request.setAttribute("nameList", nameList);

%>

<div class="jumbotron">
    <div class="container">
        <h1>Hello, world!</h1>

        <p>This is a template for a simple marketing or informational website. It includes a large callout called a
            jumbotron and three supporting pieces of content. Use it as a starting point to create something more
            unique.</p>

        <p><a class="btn btn-primary btn-lg" href="#" role="button">Learn more »</a></p>
    </div>
</div>
<div class="container">
    <button type="button" class="btn-danger btn">

    </button>
</div>

<script src="../js/bootstrap.min.js"></script>
</body>
</html>