package com.agroprosperis.study;

import com.agroprosperis.study.commands.Icommand;
import com.agroprosperis.study.exceptions.BadCommandException;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by ruslan on 17.02.16.
 */
public class AppTest {

    @Test
    public void TestMethod(){
        Assert.assertEquals(true, true);
        //Assert.assertNotNull("");
    }

    @Test(expected = ArithmeticException.class)
    public void TestBadException() throws BadCommandException {
        Executor exex = new Executor(new Icommand() {
            public void execute() throws BadCommandException {
                double a = 1/0;
            }
        });
    }
}
