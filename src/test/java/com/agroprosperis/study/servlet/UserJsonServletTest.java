package com.agroprosperis.study.servlet;

import junit.framework.TestCase;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;

/**
 * Created by ruslan on 18.02.16.
 */
public class UserJsonServletTest extends Mockito {

    @Test
    public void getUser() throws IOException {

        ByteArrayOutputStream output = new ByteArrayOutputStream();
        PrintWriter printWriter = new PrintWriter(new OutputStreamWriter(output));

        HttpServletResponse mockResponse = mock(HttpServletResponse.class);
        HttpServletRequest mockRequest = mock(HttpServletRequest.class);

        when(mockResponse.getWriter()).thenReturn(printWriter);

        UserJsonServlet servlet = new UserJsonServlet();
        try {
            servlet.doGet(mockRequest, mockResponse);
        } catch (ServletException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        String result = new String(output.toByteArray(), "UTF-8");
        Assert.assertTrue(result != null);
    }
}